=== Pnlaseia Donations ===
Contributors: Paul Osinga
License: GPL-2.0
Tags:
Requires at least: 4.0
Tested up to: 4.9
Stable tag: 0.1.0
Requires PHP: ^5.0

Donaciones ePayCo de PNL ASEIA

== Description ==

A new WordPress plugin generated with Yeoman.

== Installation ==

=== From within WordPress ===

1. Visit 'Plugins > Add New'
1. Search for 'Pnlaseia Donations'
1. Activate Pnlaseia Donations from your Plugins page.
1. Go to "after activation" below.

=== Manually ===

1. Upload the `wordpress-seo` folder to the `/wp-content/plugins/` directory
1. Activate the Pnlaseia Donations plugin through the 'Plugins' menu in WordPress
1. Go to "after activation" below.


