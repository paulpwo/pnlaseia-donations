<?php

/**
 * Pnlaseia Donations
 *
 * @package     Pnlaseia Donations
 * @author      Paul Osinga
 * @copyright   2020 Pnlaseia Donations
 * @license     GPL-2.0
 *
 * Plugin Name: Pnlaseia Donations
 * Description: Donaciones ePayCo de PNL ASEIA
 * Version:     0.1.0
 * Author:      Paul Osinga
 * Text Domain: pnlaseia-donations
 * License:     GPL-2.0
 *
 */

// Block direct access to file
defined( 'ABSPATH' ) or die( 'Not Authorized!' );

// Plugin Defines
define( "PNLASEIA_DONATIONS_FILE", __FILE__ );
define( "PNLASEIA_DONATIONS_DIR", dirname(__FILE__) );
define( "PNLASEIA_DONATIONS_INCLUDE_DIR", dirname(__FILE__) . '/include' );
define( "PNLASEIA_DONATIONS_DIR_BASENAME", plugin_basename( __FILE__ ) );
define( "PNLASEIA_DONATIONS_DIR_PATH", plugin_dir_path( __FILE__ ) );
define( "PNLASEIA_DONATIONS_DIR_URL", plugins_url( null, __FILE__ ) );

// Require the main class file
require_once( dirname(__FILE__) . '/include/class-main.php' );
