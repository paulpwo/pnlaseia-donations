<?php

// Block direct access to file
defined( 'ABSPATH' ) or die( 'Not Authorized!' );

/**
  * Donations_Pnlaseia_Submenu_Page
  *
  * @since 0.1.0
  *
  * @author Paul Osinga
  * @license GPL-2.0
  *
  */
class Donations_Pnlaseia_Submenu_Page {

  function __construct() {

    add_submenu_page(
      "",
      __("Donations pnlaseia", "pnlaseia-donations"),
      __("Donations pnlaseia", "pnlaseia-donations"),
      "administrator",
      "donations-pnlaseia",
      array($this, "print_page")
    );

  }

  public function print_page() {
    ob_start(); ?>

    <div class="wrap">

      <div class="card">
        <h1><?php _e( 'Donations pnlaseia', 'pnlaseia-donations' ); ?></h1>
        <p><?php _e( 'This is the Donations pnlaseia sub menu page start here to customize your template.', 'pnlaseia-donations' ); ?></p>
      </div>

    </div><?php

    return print(ob_get_clean());
  }

}
