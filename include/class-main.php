<?php

// Block direct access to file
defined( 'ABSPATH' ) or die( 'Not Authorized!' );

class Pnlaseia_Donations {

	private $settings;

	private $metaboxes = array();

	private $widgets = array();

	private $shortcodes = array();

	private $toolbars = array();

	private $taxonomies = array();

	public function __construct() {

		// Plugin uninstall hook
		register_uninstall_hook( PNLASEIA_DONATIONS_FILE, array(__CLASS__, 'plugin_uninstall') );

		// Plugin activation/deactivation hooks
		register_activation_hook( PNLASEIA_DONATIONS_FILE, array($this, 'plugin_activate') );
		register_deactivation_hook( PNLASEIA_DONATIONS_FILE, array($this, 'plugin_deactivate') );

		// Plugin Actions
		add_action( 'plugins_loaded', array($this, 'plugin_init') );

		// User
		add_action( 'wp_enqueue_scripts', array($this, 'plugin_enqueue_scripts') );

		// Admin
		add_filter( 'mce_css', array($this, 'plugin_add_editor_style') );
		add_action( 'admin_enqueue_scripts', array($this, 'plugin_enqueue_admin_scripts') );
		add_action( 'admin_init', array($this, 'plugin_register_settings') );
		add_action( 'admin_menu', array($this, 'plugin_add_settings_pages') );

		// Register plugin widgets
		add_action( 'widgets_init', function(){
			foreach ($this->widgets as $widgetName => $widgetPath) {
				include_once( PNLASEIA_DONATIONS_INCLUDE_DIR . $widgetPath );
				register_widget( $widgetName );
			}
		});

		// Init plugin shortcodes
		foreach ($this->shortcodes as $className => $path) {
			include_once( PNLASEIA_DONATIONS_INCLUDE_DIR . $path );
			new $className();
		}

		// Init plugin metaboxes
		foreach ($this->metaboxes as $className => $path) {
			include_once( PNLASEIA_DONATIONS_INCLUDE_DIR . $path );
			new $className();
		}

		// Init plugin taxonomies
		foreach ($this->taxonomies as $className => $path) {
			include_once( PNLASEIA_DONATIONS_INCLUDE_DIR . $path );
			new $className();
		}

		// Init plugin toolbars
		foreach ($this->toolbars as $className => $path) {
			include_once( PNLASEIA_DONATIONS_INCLUDE_DIR . $path );
			new $className();
		}

	}

	/**
	* Plugin uninstall function
	* called when the plugin is uninstalled
	* @method plugin_uninstall
	*/
	public static function plugin_uninstall() { }

	/**
	* Plugin activation function
	* called when the plugin is activated
	* @method plugin_activate
	*/
	public function plugin_activate() { }

	/**
	* Plugin deactivate function
	* is called during plugin deactivation
	* @method plugin_deactivate
	*/
	public function plugin_deactivate() { }

	/**
	* Plugin init function
	* init the polugin textDomain
	* @method plugin_init
	*/
	function plugin_init() {
		load_plugin_textDomain( 'pnlaseia-donations', false, dirname(PNLASEIA_DONATIONS_DIR_BASENAME) . '/languages' );
		add_action('wp_footer', array($this, 'loadBotton'));
	}

	function loadBotton(){	
		$this->settings = get_option( 'pnlaseia-donations_main_options' );

		if($this->settings){
			echo $this->settings['first_option'];
		}

	}

	/**
	* Add the plugin menu page(s)
	* @method plugin_add_settings_pages
	*/
	function plugin_add_settings_pages() {

		add_menu_page(
			__('Pnlaseia Donations', 'pnlaseia-donations'),
			__('Pnlaseia Donations', 'pnlaseia-donations'),
			'administrator', // Menu page capabilities
			'pnlaseia-donations-settings', // Page ID
			array($this, 'plugin_settings_page'), // Callback
			'dashicons-admin-generic',
			null
		);

	}

	/**
	* Register the main Plugin Settings
	* @method plugin_register_settings
	*/
	function plugin_register_settings() {

		register_setting( 'pnlaseia-donations-settings-group', 'pnlaseia-donations_main_options', array($this, 'plugin_sanitize_settings') );

		add_settings_section( 'main', __('Main Settings', 'pnlaseia-donations'), array( $this, 'main_section_callback' ), 'pnlaseia-donations-settings' );

		add_settings_field( 'first_option', 'Código', array( $this, 'first_option_callback' ), 'pnlaseia-donations-settings', 'main' );

	}

	/**
	* The text to display as description for the main section
	* @method main_section_callback
	*/
	function main_section_callback() {
		return _e( 'Pegue aqui el codigo del boton.<br> Es importante recordar que la imagen debe estar correcta en las variables y la pagina de respuesta predetarminada es <b>Gracias</b>, si esta no existe, debe ser creada', 'pnlaseia-donations' );
	}

	/**
	* Create the option html input
	* @return html
	*/
	function first_option_callback() {
		return printf(
			'<textarea id="first_option" name="pnlaseia-donations_main_options[first_option]" cols="120" rows="70">%s</textarea><div id="first_option_editor"/>',
			isset( $this->settings['first_option'] ) ? esc_attr( $this->settings['first_option']) : ''
		);
	}

	/**
	* Sanitize the settings values before saving it
	* @param  mixed $input The settings value
	* @return mixed        The sanitized value
	*/
	function plugin_sanitize_settings($input) {
		return $input;
	}

	/**
	* Enqueue the main Plugin admin scripts and styles
	* @method plugin_enqueue_scripts
	*/
	function plugin_enqueue_admin_scripts() {

		wp_register_style(
			'pnlaseia-donations_admin_style',
			PNLASEIA_DONATIONS_DIR_URL . '/assets/dist/admin.css',
			array(),
			null
		);

		wp_register_script(
			'pnlaseia-donations_admin_script',
			PNLASEIA_DONATIONS_DIR_URL . "/assets/dist/admin.js",
			array('jquery'),
			null,
			true
		);
		wp_enqueue_style('pnlaseia-donations_admin_style');
		wp_enqueue_script('pnlaseia-donations_admin_script');
		

	}

	/**
	* Enqueue the main Plugin user scripts and styles
	* @method plugin_enqueue_scripts
	*/
	function plugin_enqueue_scripts() {

		wp_register_style(
			"pnlaseia-donations_user_style",
			PNLASEIA_DONATIONS_DIR_URL . "/assets/dist/user.css",
			array(),
			null
		);

		wp_register_script(
			"pnlaseia-donations_user_script",
			PNLASEIA_DONATIONS_DIR_URL . "/assets/dist/user.js",
			array('jquery'),
			null,
			true
		);

		wp_enqueue_style('pnlaseia-donations_user_style');
		wp_enqueue_script('pnlaseia-donations_user_script');

	}

	/**
	* Add the plugin style to tinymce editor
	* @method plugin_add_editor_style
	*/
	function plugin_add_editor_style($styles) {
		if ( !empty( $styles ) ) {
			$styles .= ',';
		}
		$styles .= PNLASEIA_DONATIONS_DIR_URL . '/assets/dist/editor-style.css';
		return $styles;
	}

	/**
	* Plugin main settings page
	* @method plugin_settings_page
	*/
	function plugin_settings_page() {

		ob_start(); ?>

		<div class="wrap" style="max-width: 860px;">

			<div class="card" style="max-width: 860px;">

				<h1><?php _e( 'Pnlaseia Donations', 'pnlaseia-donations' ); ?></h1>

				<p><?php _e( 'Configuración del boton de donaciones', 'pnlaseia-donations' ); ?></p>

			</div>
			<style>
			#first_option_editor {
				    width: 860px;
    				height: 465px;
			}
			</style>
			<div class="card" style="max-width: 860px;">
			<script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.4.12/ace.min.js" integrity="sha512-GoORoNnxst42zE3rYPj4bNBm0Q6ZRXKNH2D9nEmNvVF/z24ywVnijAWVi/09iBiVDQVf3UlZHpzhAJIdd9BXqw==" crossorigin="anonymous"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.4.12/mode-javascript.min.js" integrity="sha512-ZxMbXDxB0Whct+zt+DeW/RZaBv33N5D7myNFtBGiqpDSFRLxn2CNp6An0A1zUAJU/+bl8CMVrwxwnFcpFi3yTQ==" crossorigin="anonymous"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.4.12/theme-monokai.min.js" integrity="sha512-S4i/WUGRs22+8rjUVu4kBjfNuBNp8GVsgcK2lbaFdws4q6TF3Nd00LxqnHhuxS9iVDfNcUh0h6OxFUMP5DBD+g==" crossorigin="anonymous"></script>
				<?php $this->settings = get_option( 'pnlaseia-donations_main_options' ); ?>

				<form method="post" action="options.php">

					<?php settings_fields( 'pnlaseia-donations-settings-group' ); ?>
					<?php do_settings_sections( 'pnlaseia-donations-settings' ); ?>

					<?php submit_button(); ?>

				</form>

			</div>
			<script>
			//const editor = ace.edit("first_option");
			//ace.config.setModuleUrl('ace/mode/javascript', require('file-loader!.//cdnjs.cloudflare.com/ajax/libs/ace/1.4.12/mode-javascript.min.j'))
			//var JavaScriptMode = ace.require("ace/mode/javascript").Mode;
			//editor.session.setMode(new JavaScriptMode());

				var editor = ace.edit("first_option_editor");
				var JavaScriptMode = ace.require("ace/mode/javascript").Mode;
				editor.session.setMode(new JavaScriptMode());
				editor.setTheme("ace/theme/monokai");
				document.getElementById('first_option_editor').style.fontSize='14px';

				var textarea = jQuery('textarea[id="first_option"]').hide();
				editor.getSession().setValue(textarea.val());
				editor.getSession().on('change', function(){
					textarea.val(editor.getSession().getValue());
				});
				editor.session.on('change', function(delta) {
					// delta.start, delta.end, delta.lines, delta.action
					textarea.val(editor.getSession().getValue());
				});

			</script>

		</div><?php

		return print( ob_get_clean() );

	}

}

new Pnlaseia_Donations;
